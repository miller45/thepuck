﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PuckController : MonoBehaviour
{
    private Rigidbody2D rigidbody;
    private bool startDragging;
    private Vector2 startDragPosition;

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            RaycastHit2D hit = Physics2D.Raycast(
                Camera.main.ScreenToWorldPoint(Input.mousePosition),
                Vector3.back,
                15);

            // проверяем попадет луч на шайбу или нет
            if (hit.collider != null
                && hit.collider.gameObject.tag == "Puck")
            {
                startDragging = true;
                startDragPosition = hit.point;
            }
        }

        if (startDragging
            && Input.GetKey(KeyCode.Mouse0))
        {
            RaycastHit2D hit = Physics2D.Raycast(
                Camera.main.ScreenToWorldPoint(Input.mousePosition),
                Vector3.back,
                15);

            if (startDragging
                && hit.collider != null
                && hit.collider.gameObject.tag == "PlayingField")
            {
                Vector2 direction = hit.point - startDragPosition;
                rigidbody.AddForce(direction * 130,ForceMode2D.Impulse);
                startDragging = false;
            }

        }

        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            startDragging = false;
        }
    }
}
