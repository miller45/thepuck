﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour
{
    [SerializeField] private GameController gameController;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Puck")
        {
            gameController.NextLevel();
        }
    }
}
