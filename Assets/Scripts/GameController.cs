﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameAnalyticsSDK;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameController : MonoBehaviour
{
    [SerializeField] private Text level;
    [SerializeField] private GameObject gate;
    [SerializeField] private GameObject puck;
    [SerializeField] private Vector3 startPuckPosition;
    [SerializeField] private Vector3 startGatePosition;
    [SerializeField] private GameObject enemy;
    private int currentLevel = 1;
    
    private List<Vector3[]> enemyPositions = new List<Vector3[]>
    {
         new[] {new Vector3(0,0,-1) },
         new[] {new Vector3(-1,0,-1), new Vector3(1,0,-1)},
         new[] {new Vector3(0,-1,-1), new Vector3(1,2,-1),  new Vector3(-1,1,-1)},
         new[] {new Vector3(1,-1,-1), new Vector3(1,1,-1),  new Vector3(-1,1,-1),new Vector3(-1,-1,-1)},
         new[] {new Vector3(1.5f,-2f,-1), new Vector3(1.5f,2,-1),  new Vector3(-1,2,-1),new Vector3(-1,-1.2f,-1) ,new Vector3(0,0,-1)},
    };

    private void Awake()
    {
        GameAnalytics.Initialize();
    }

    private void Start()
    {
        StartLevel(currentLevel);
    }

    private void StartLevel(int level)
    {
        DestroyEnemies();
        this.level.text = "Level " + level;
        gate.transform.position = new Vector3(
            startGatePosition.x * Random.Range(-1.25f, 1.25f),
            startGatePosition.y,
            startGatePosition.z);
        puck.transform.position = startPuckPosition;
        puck.GetComponent<Rigidbody2D>().Sleep();

        for (int i = 0; i < enemyPositions[currentLevel-1].Length; i++)
        {
            Instantiate(enemy, enemyPositions[currentLevel-1][i], Quaternion.identity);
        }
    }

    public void NextLevel()
    {
        GameAnalytics.NewDesignEvent("Level " + currentLevel + " finished");
        currentLevel++;
        if (currentLevel > 5)
        {
            currentLevel = 1;
        }
        StartLevel(currentLevel);
    }

    public void Restart()
    {
        currentLevel = 1;
        StartLevel(currentLevel);
    }

    private void DestroyEnemies()
    {
        Enemy[] enemies = FindObjectsOfType<Enemy>();
        for (int i = enemies.Length-1; i > -1; i--)
        {
            Destroy(enemies[i].gameObject);
        }
    }
}
